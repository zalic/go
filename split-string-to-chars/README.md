# Split string to chars

```go
package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.Split("string", "")) // [s t r i n g]
	fmt.Println(strings.Split("öö", "")) // [ö ö]
}
```

```go
package main

import (
	"fmt"
)


func usingFor(s string) {
	// byte is an alias for uint8,
	// so is valid but rude to use - []uint8{}
	chars := []byte{}
	for i := 0; i < len(s); i++ {
		// c has a type "byte"
		c := s[i]
		// fmt.Println(c == 's')
		chars = append(chars, c)
	}

	// if you just want the array you can skip foreach and simply use
	// chars := []byte(s)

	fmt.Println(chars)                       // [115 116 114 105 110 103]
	fmt.Println(fmt.Sprintf("%c", chars[0])) // s
}

func usingRange(s string) {
	// rune is alias for int32,
	// so it's valid but rude to use - []int32{}
	chars := []rune{}
	for _, c := range s {
		// c has a type "rune"
		// https://go.dev/doc/go1#rune
		// c == 's'
		// c == 't'
		chars = append(chars, c)
	}

	// if you just want the array you can skip foreach and simply use
	// chars := []rune(s)

	fmt.Println(chars)                       // [115 116 114 105 110 103]
	fmt.Println(fmt.Sprintf("%c", chars[0])) // s
}

func main() {
	usingFor("string")
	usingRange("string")
}

```
`ö`
 - as bytes `[195 182]` (`Ã¶`)
 - as runes `[246]` (`ö`)


Single quotes are used to declare either a byte or a rune.
```go
func main() {
	c := 'x'
	fmt.Println(c) // 120
	fmt.Printf("%c", c) // x
}
```

substr
```go
input := "öö"
sub := string([]rune(input)[0:1])
fmt.Println(sub) // ö
```


