/*
for
https://go.dev/tour/methods/25
refs
https://go.dev/tour/moretypes/18
https://go.dev/tour/methods/24
*/
package main

import (
	"image"
	"image/color"

	"golang.org/x/tour/pic"
)

type Image struct {
	width  int
	height int
	data   [][]uint8
}

func (i Image) ColorModel() color.Model {
	return color.RGBAModel
}

func (i Image) Bounds() image.Rectangle {
	result := image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: i.width, Y: i.height}}
	return result
}

func (i Image) At(x, y int) color.Color {
	// return color.Gray{150}
	return color.RGBA{255, i.data[x][y], 0, 255}
}

func (i *Image) Draw(f func(x, y int) uint8) {
	r := [][]uint8{}
	for x := 0; x <= i.width; x++ {
		t := []uint8{}
		for y := 0; y <= i.height; y++ {
			// t = append(t, uint8(x^y))
			t = append(t, f(x, y))
		}
		r = append(r, t)
	}
	i.data = r
}

func main() {
	m := Image{width: 100, height: 100}
	//m.Draw(func(x, y int) uint8 { return uint8(x * y) })
	//m.Draw(func(x, y int) uint8 { return uint8((x + y) / 2) })
	m.Draw(func(x, y int) uint8 { return uint8(x ^ y) })
	pic.ShowImage(m)
}
