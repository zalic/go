# The Go Programming Language

[[_TOC_]]

## refs
- https://go.dev/doc/
- [Language Specification](https://go.dev/ref/spec)
- [builtin](https://pkg.go.dev/builtin) - predeclared identifiers
- [Go's Declaration Syntax](https://go.dev/blog/declaration-syntax)
- [Standard library](https://pkg.go.dev/std)
  - [slog](https://pkg.go.dev/log/slog) -  [Structured Logging with slog](https://go.dev/blog/slog)
- **[CLI](https://pkg.go.dev/cmd)**
  - [go](https://pkg.go.dev/cmd/go) for managing Go source code
  - [gofmt](https://pkg.go.dev/cmd/gofmt) formats Go programs
- [Defer, Panic, and Recover](https://go.dev/blog/defer-panic-and-recover)
- [Effective Go](https://go.dev/doc/effective_go) - Tips for writing clear, performant, and idiomatic Go code
- Go Proverbs
  - https://go-proverbs.github.io/
  - https://www.youtube.com/watch?v=PAAkCSZUG1c
- **Tutorials**
  - https://go.dev/doc/tutorial/
  - [Learn Go in 12 Minutes](https://www.youtube.com/watch?v=C8LgvuEBraI) (part 1)
  - [Concurrency in Go](https://www.youtube.com/watch?v=LvgVSSpwND8) (part 2)
  - https://semaphoreci.com/community/tutorials/how-to-deploy-a-go-web-application-with-docker
  - https://fireship.io/lessons/learn-go-in-100-lines/
- **Modules**
  - [fetch](https://pkg.go.dev/github.com/go-zoox/fetch) - HTTP Client
  - [chi](https://github.com/go-chi/chi) - lightweight, idiomatic and composable router for building Go HTTP services
  - [Fiber](https://github.com/gofiber/fiber) - Express inspired web framework
  - [GORM](https://gorm.io/index.html) - ORM library
  - [TEMPL](https://github.com/a-h/templ) - HTML templating language
  - **Swagger**
    - [Swagger 2.0](https://github.com/go-swagger/go-swagger)
    - https://kecci.medium.com/swagger-open-api-specification-2-0-and-3-0-in-go-c1f05b51a595
  - **GraphQL**
    - https://www.apollographql.com/blog/graphql/golang/using-graphql-with-golang/
    - https://hasura.io/blog/building-a-graphql-api-with-golang-postgres-and-hasura/
	- https://medium.com/safetycultureengineering/why-we-moved-our-graphql-server-from-node-js-to-golang-645b00571535

## set up dev env

```bash
$ mkdir `go env GOPATH`
$ cd `go env GOPATH`
$ mkdir src
$ cd src
$ mkdir yourproject
$ cd yourproject
$ touch main.go
...
$ go run main.go
$ go build # creates executable ( ./main )
$ go install # build executable in GOPATH/bin/yourproject with dependencies
```

## base file setup

Every Go program is made up of packages.
Programs start running in package main.

```go
package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello")
}
```

By convention, the package name is the same as the last element of the import path.
For instance, the `"math/rand"` package comprises files that begin with the statement `package rand`.

```go
package main

import (
	"fmt"
	"math/rand"
)

func main() {
	fmt.Println("My favorite number is", rand.Intn(10))
}
```

In Go, a name is exported if it begins with a capital letter.

```go
package main

import (
	"fmt"
	"math"
)

func main() {
	// fmt.Println( math.pi ) // ./prog.go:10:20: undefined: math.pi
	fmt.Println( math.Pi )
}
```

A directory of Go code can have at most one package.
All `.go` files in a single directory must all belong to the same package.
If they don't an error will be thrown by the compiler.
This is true for main and library packages alike.

## Variables

The var statement declares a list of variables;
the type is last.

```go
var x int = 1
// short assignment - can only be used inside a function
y := 1

// three booleans
var isFoo, isBar, isFoobar bool

// two integers
var i, j int = 1, 2

// two booleans and a string
var isFoo, isBar, foobar = true, false, "no!"
```

### Variable types
```
bool
string
int  int8  int16  int32  int64
uint uint8 uint16 uint32 uint64 uintptr
byte // alias for uint8
rune // alias for int32, represents a Unicode code point
float32 float64
complex64 complex128
```
- https://go.dev/ref/spec#Types
  - https://go.dev/ref/spec#Numeric_types

Variables declared without an explicit initial value are given their _zero value_.
- `0` for numeric types,
- `false` for the boolean type, and
- `""` (the empty string) for strings.

Print out variable type
```go
a := 1
fmt.Printf("%T", a) // int
fmt.Println(reflect.TypeOf(a)) // int
```

Type conversions - The expression T(v) converts the value v to the type T.
```go
var i int = 42
var f float64 = float64(i)
var u uint = uint(f)

i := 42
f := float64(i)
u := uint(f)
```

converting string to int
- https://pkg.go.dev/strconv
```go
var a string = "100"
var atoi, _ = strconv.Atoi(a)
var base8, _ = strconv.ParseInt(a, 8, 64)
var base10, _ = strconv.ParseInt(a, 10, 64)
fmt.Println(atoi, fmt.Sprintf("%T", atoi))     // 100 int64
fmt.Println(base8, fmt.Sprintf("%T", base8))   // 64 int64
fmt.Println(base10, fmt.Sprintf("%T", base10)) // 100 int64
```

type switch
```go
var a interface{} = 1
switch a := a.(type) {
case int:
    fmt.Println("int", a)
}
```

### Type assertion
- https://go.dev/tour/methods/15


- `t := i.(int)`
  - If `i` holds a `T`, then `t` will be the underlying value
  - If `i` does not hold a `T`, the statement will trigger a panic.

```go
var a interface{} = 1
ti := a.(int)
fmt.Println("ti", ti) // 1
fmt.Println(fmt.Sprintf("%T", ti)) // int


ts := a.(string)
fmt.Println(ts)
// would result in
// panic: interface conversion: interface {} is int, not string
```

To test whether an interface value holds a specific type,
a type assertion can return two values:
the underlying value and a boolean value that reports whether the assertion succeeded.
- `t, ok := i.(T)`
  - If `i` holds a `T`, then `t` will be the underlying value and ok will be true
  - If not, `ok` will be false and `t` will be the zero value of type `T`.


```go
var a interface{} = "test"

ti, ok := a.(int)
if ok {
    fmt.Println("the value is an int", ti)
} else {
  // because a was not int
  // ti is a new int variable with int zero value ( 0 )
  fmt.Println("ti", ti) // will print 0
}
ts, ok := a.(string)
if ok {
    fmt.Println("the value is a string", ts)
}
```
we can streamline it by discarding the return value
and storing variables in if clause
```go
if _, ok := a.(int); ok {
    fmt.Println("the value is an int", a)
}
if _, ok := a.(string); ok {
    fmt.Println("the value is a string", a)
}
```
```go
var a interface{} = ""

a, ok := a.(string)
// if a was not a string then now it is - value is types zero value
// string zero value is empty string
if ok {
    fmt.Println("the value is an string")
}
```

## Pointers

Go has pointers. A pointer holds the memory address of a value.

The type `*T` is a pointer to a `T` value. Its zero value is `nil`.

```go
var p *int
```

The `&` operator generates a pointer to its operand.

```go
i := 42
p = &i
```

The `*` operator denotes the pointer's underlying value.

```go
fmt.Println(*p) // read i through the pointer p
*p = 21         // set i through the pointer p
```
This is known as "dereferencing" or "indirecting".

```go
package main

import "fmt"

func main() {
	i, j := 42, 2701

	p := &i         // point to i
	fmt.Println(p)  // pointer ( 0xc00001c030 )
	fmt.Println(*p) // read i through the pointer ( 42 )
	*p = 21         // set i through the pointer
	fmt.Println(i)  // see the new value of i ( 21 )

	p = &j          // point to j
	*p = *p / 37    // divide j through the pointer
	fmt.Println(j)  // see the new value of j ( 73 )
}
```

```go
package main

import (
	"fmt"
)

func a(nr int) {
	nr++
}

func b(nr *int) {
	*nr++
}

func main() {
	nr := 1
	a(nr)
	fmt.Println(nr) // 1
	b(&nr)
	fmt.Println(nr) // 2
}
```

## array
Array holds multiple elements of the same type - number of elements array holds is fixed.

```go
var a [5]int // array with five ints ( [0 0 0 0 0] )

a[2] = 7 // [0 0 7 0 0]

b := [5]int{4, 9, 2, 6, 3} // [4 9 2 6 3]
c := [5]int{4, 2} // [4 2 0 0 0]
```

It is an error to provide elements with index values outside the index range of the array.
The notation `...` specifies an array length equal to the maximum element index plus one.

```go
foo := [...]string{"a", "b", "c"} // array with len(3)
bar := []string{"a", "b", "c"} // slice with len(3)
```

- [Arrays vs Slices](https://www.godesignpatterns.com/2014/05/arrays-vs-slices.html)
  - assigning one array to another copies all of the elements
  - if you pass an array to a function, it will receive a copy of the array
  - slices can be resized using the built-in append function
  - slices are _reference types_, meaning that they are cheap to assign and can be passed to other functions without having to create a new copy of its underlying array
  - **Overall, slices are cleaner, more flexible, and less bug-prone than arrays, so you should prefer using them over arrays whenever possible.**


### slices
- https://go.dev/ref/spec#Slice_type
- https://go.dev/blog/slices-intro
- https://www.digitalocean.com/community/tutorials/understanding-arrays-and-slices-in-go

Abstraction on top of arrays - doesn't have a fixed length

Slices can contain any type, including other slices.

```go
var x []int {4, 2} // [4 2]

// append creates a new array with the new element added to it
// func append(slice []Type, elems ...Type) []Type
x = append(a, 13) // [4 2 13]
x = append(a, 1, 2, 3) // [4 2 13 1 2 3]

// storing new array in new variable
a := []int{4, 2}
var b []int
b = append(a, 6)
fmt.Println(a, b) // [4 2] [4 2 6]
```

### Slice expressions

- https://go.dev/ref/spec#Slice_expressions

Slice expressions construct a substring or slice
from a string, array, pointer to array, or slice.

```go
a[low : high]

a := [5]int{1, 2, 3, 4, 5}
s := a[1:4]

//  s has type []int, length 3, capacity 4, and elements
s[0] == 2
s[1] == 3
s[2] == 4
```

```go
arr := []string{"a", "b", "c", "d"}
fmt.Println(arr[2:4])                    // c, d
fmt.Println(append(arr[:1], arr[3:]...)) // a, d
fmt.Println(append(arr[:2], arr[3:]...)) // a, b, d
```

A slice does not store any data, it just describes a section of an underlying array.

Changing the elements of a slice modifies the corresponding elements of its underlying array.

Other slices that share the same underlying array will see those changes.

```go
arr := []string{"a", "b", "c", "d"}

sli1 := arr[0:1]
fmt.Println(sli1) // [a]

sli2 := arr[2:4]
fmt.Println(sli2) // [c, d]

arr[0] = "x"

fmt.Println(arr)  // [x, b, c, d]
fmt.Println(sli1) // [x]
```

### Slice length and capacity

```go
package main

import "fmt"

func main() {
	s := []int{2, 3, 5, 7, 11, 13}
	printSlice(s) // len=6 cap=6 [2 3 5 7 11 13]

	// Slice the slice to give it zero length.
	s = s[:0]
	printSlice(s) // len=0 cap=6 []

	// Extend its length.
	s = s[:4]
	printSlice(s) // len=4 cap=6 [2 3 5 7]

	// Drop its first two values.
	s = s[2:]
	printSlice(s) // len=2 cap=4 [5 7]

	// note that the first two values have now been removed
	// slice can now only grow to a lenght of four
	s = s[:4]
	printSlice(s) // len=4 cap=4 [5 7 11 13]
}

func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}
```

### Using make to create slice
```go
// make([]T, length[, capacity])

a := make([]int, 5)
printSlice("a", a) // a len=5 cap=5 [0 0 0 0 0]

b := make([]int, 0, 5)
printSlice("b", b) // b len=0 cap=5 []

c := b[:2]
printSlice("c", c) // c len=2 cap=5 [0 0]
```



## map

```go
// map[KEYS]VALUES
mymap := make(map[string]int)
mymap["foo"] = 1
mymap["bar"] = 2

fmt.Println(mymap) // map[bar:2 foo:1]

fmt.Println(mymap["bar"]) // 2

delete(mymap, "foo")
fmt.Println(mymap) // map[bar:2]

// Test that a key is present with a two-value assignment
// If key is in m, ok is true. If not, ok is false.
// If key is not in the map, then elem is the zero value for the map's element type.
elem, ok := mymap["bar"]
fmt.Println(elem, ok) // 2, true
elem, ok = mymap["foo"]
fmt.Println(elem, ok) // 0 false

```

## if / else if / else

```go
x := 5

if x > 6 {

} else if x < 2 {

} else {

}
```
If statement can start with a short statement to execute before the condition.

```go
    a := 1
    if b := a + 1; b > 1 {
        fmt.Println("Foo")
    }
```
Variables declared inside an if short statement are also available inside any of the else blocks.
```go
    a := 1
    if b := a + 1; b > 1 {
        fmt.Println("larger than one:", b)
    } else {
        fmt.Println("not larger than one:", b)
    }
    // fmt.Println("undefined here", b) // would give error "undefined: b"
```

## switch

```go
package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Print("Go runs on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		// freebsd, openbsd,
		// plan9, windows...
		fmt.Printf("%s.\n", os)
	}
}
```

Switch cases evaluate cases from top to bottom, stopping when a case succeeds.

```go
package main

import (
	"fmt"
)

func f(i int) int {
	fmt.Println("f function called")
	return i
}

func foo(i int) {
	switch i {
		case 0:
			fmt.Println("foo 0")
		case f(i):
			fmt.Println("foo 1")
	}
}


func main() {
	foo(0) // outputs "foo 0"
	foo(1) // outputs "f function called; foo 1"
}
```

Switch without a condition is the same as `switch true`.

This construct can be a clean way to write long if-then-else chains.

```go
package main

import (
	"fmt"
	"time"
)

func main() {
	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("Good morning!")
	case t.Hour() < 17:
		fmt.Println("Good afternoon.")
	default:
		fmt.Println("Good evening.")
	}
}
```

## Functions

```go
package main

import "fmt"

func add(x int, y int) int {
	return x + y
}

func main() {
	fmt.Println(add(42, 13))
}
```

When two or more consecutive named function parameters share a type,
you can omit the type from all but the last.

```go
func add(x, y int) int {
```

A function can return any number of results.

```go
package main

import "fmt"

func swap(x, y string) (string, string) {
	return y, x
}

func main() {
	a, b := swap("hello", "world")
	fmt.Println(a, b)
}
```

Go's return values may be named. If so, they are treated as variables defined at the top of the function.

These names should be used to document the meaning of the return values.

A return statement without arguments returns the named return values. **This is known as a "naked" return.**

```go
package main

import "fmt"

func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return
}

func main() {
	fmt.Println(split(17)) // prints "7 10"
}
```

Functions can also be assigned as variables

Functions can be used as function arguments
```go
func test(f func(int, int) int, n int) int {
	return f(n, n)
}

func main() {
  add := func(a, b int) int {
      return a + b
  }
  fmt.Println(test(add, 3))

  // an inline function
  fmt.Println(test(func(a, b int) int { return a * b }, 3))
}
```

```go
func increment(i int) {
  // incrementing the local copy
  i++
}
func increment2(i *int) {
  // incrementing original value via pointer
  *i++
}
func main() {
	i := 1
	increment(i)
	fmt.Println(i) // 1

	increment2(&i)
	fmt.Println(i) // 2
}
```

## loops

```go
for i := 0; i < 5; i++ {
  fmt.Println(i)
}
```

```go
i := 0

for i < 5 {
  fmt.Println(i)
  i++
}
```
If you omit the loop condition it loops forever, so an infinite loop is compactly expressed.
```go
for {
}
```

```go
arr := []string{"a", "b", "c"}

for index, value := range arr {
  fmt.Println("index", index, "value", value)
}
```

```go
m := make(map[string]string)
m["a"] = "apple"
m["b"] = "banana"

for index, value := range m {
  fmt.Println("index", index, "value", value)
}
```

You can skip the index or value by assigning to `_`.
```go
for _, value := range m {
for index, _ := range m {
```
If you only want the index, you can omit the second variable.
```go
for index := range m {
```

## errors
- [Error handling and Go](https://go.dev/blog/error-handling-and-go)

```go
package main

import (
    "fmt"
    "errors"
)

func main() {
    result, err := divide(8, 0)

    if (err != nil) {
        fmt.Println(err)
    } else {
        fmt.Println(result)
    }
}

func divide(x, y int) (int, error) {
    if (y == 0) {
        return 0, errors.New("Can't divide by zero")
    }
    return x / y, nil
}
```

Error is any type that implements the [error interface](https://go.dev/blog/error-handling-and-go#the-error-type)
```go
type error interface {
    Error() string
}
```

```go
type NegativeIntegerError struct {
	number int
}
func (e NegativeIntegerError) Error() string {
	return fmt.Sprintf("%d is a negative integer", e.number)
}
func foo(n int) (int, error) {
	if n < 0 {
		return 0, NegativeIntegerError{n}
	} else {
		return n, nil
	}
}
// or as type int
type NegativeIntegerError int

func (e NegativeIntegerError) Error() string {
    return fmt.Sprintf("%d is a negative integer", e)
}
func foo(n int) (int, error) {
  if n < 0 {
      return 0, NegativeIntegerError(n)
  } else {
      return n, nil
  }
}
```


## struct types
```go
package main

import (
    "fmt"
)

type person struct {
  name string
  age int
}

func main() {
  p := person{name: "Foo", age: 69}
  fmt.Printn(p)
  fmt.Printn(p.age)
}
```

```go
type NamedStructChild struct {
	value string
}
type NamedStructParent struct {
	child NamedStructChild
}

type UnnamedStructParent struct {
	child struct {
		value string
	}
}

func main() {
	x := struct {
		value string
	}{value: "hello"}
	fmt.Println(x.value)

	a := NamedStructParent{child: NamedStructChild{value: "apple"}}
	fmt.Println(a.child.value)

	b := UnnamedStructParent{}
	b.child.value = "banana"
	fmt.Println(b.child.value)

	// when using unnamed structs
	// you'll probably want to avoid
	// setting values when initialize variable
	b2 := UnnamedStructParent{child: struct{ value string }{value: "not-so-readable"}}
	fmt.Println(b2.child.value)
	// see https://stackoverflow.com/a/54126129
}
```

### Embedded struct

```go
type Bar struct {
  bar int
}
type Foo struct {
  Bar
  foo int
}

func main() {
  // when initializing you cant add values to embedded struct values directly
  obj := Foo{foo: 2, Bar: Bar{bar: 7}}
  // but after initialization the keys of the embedded struct
  // are on the parent struct level
  fmt.Println(obj)         // {{7} 2}
  fmt.Println(obj.bar)     // 7
  obj.bar = 9
  fmt.Println(obj.bar)     // 9
  // the value is also accessible via the long route
  fmt.Println(obj.Bar.bar) // 9
}
```

## methods

```go
type rect struct {
	width, height int
}

func (r rect) area() int {
	return r.width * r.height
}

func main() {
	r := rect{width: 2, height: 10}
	fmt.Println(r.area()) // 20
}
```

```go
type NUMBA int

func (n NUMBA) square() int {
	return int(n * n)
}

func main() {
	fmt.Println(NUMBA(5).square()) // 25
	// or
    var n NUMBA = 5
    fmt.Println(n.square()) // 25
}
```

### Pointer receivers
- https://go.dev/tour/methods/4

There are two reasons to use a pointer receiver.

The first is so that the method can modify the value that its receiver points to.

The second is to avoid copying the value on each method call.
This can be more efficient if the receiver is a large struct, for example.

In general, all methods on a given type should have
either value or pointer receivers, but not a mixture of both.

```go
// type cannot itself be a pointer such as int
type V struct {
	x int
	y int
}

func (v *V) set() {
	v.x = 1
	v.y = 2
}
func main() {
	x := V{}
	fmt.Println(x) // {0 0}
	x.set()
	fmt.Println(x) // {1 2}
}
```

## interfaces

Interfaces are named collections of method signatures.

A type implements an interface by implementing its methods.
There is no explicit declaration of intent, no "implements" keyword.

Any type can implement any number of interfaces.

- Effective Go: Interface names: https://go.dev/doc/effective_go#interface-names
- TODO: Interface values with nil underlying values: https://go.dev/tour/methods/12

```go
type shape interface {
	area() float64
	perimeter() float64
}

type rect struct {
	width, height float64
}
type circle struct {
	radius float64
}

func (r rect) area() float64 {
	return r.width * r.height
}
func (r rect) perimeter() float64 {
	return 2*r.width + 2*r.height
}

func (c circle) area() float64 {
	return math.Pi * c.radius * c.radius
}
func (c circle) perimeter() float64 {
	return 2 * math.Pi * c.radius
}

// measure only accepts types that implement the shape interface
// or "only types that have area and perimeter methods attached to them"
func measure(g shape) {
	fmt.Println(g)
	fmt.Println(g.area())
	fmt.Println(g.perimeter())
}

func main() {
	r := rect{width: 3, height: 4}
	c := circle{radius: 5}

	measure(r)
	fmt.Println("----------")
	measure(c)
	/*
		{3 4}
		12
		14
		----------
		{5}
		78.53981633974483
		31.41592653589793
	*/
}
```
interfaces can have named arguments too,
but it's just to make the interface easier to read

```go
type StringCombiner interface {
	combine(string, string) string
}
// can be written as
type StringCombiner interface {
	combine(frontPartOfTheString string, backPartOfTheString string) (combinedString string)
}
```

The interface type that specifies zero methods is known as the `empty interface`:
```go
interface{}
```
An empty interface may hold values of any type.
(Every type implements at least zero methods.)

Empty interfaces are used by code that handles values of unknown type.

#### Stringers interface

```go
type Stringer interface {
    String() string
}
```
A Stringer is a type that can describe itself as a string.
The fmt package (and many others) look for this interface to print values.

```go
type NUMBA int

func (n NUMBA) String() string {
	return fmt.Sprintf("NUMBA(%d)'", n)
}

func main() {
	x := NUMBA(7)
	fmt.Println(x) // NUMBA(7)
}
```

#### Readers interface

- https://go.dev/tour/methods/21

```go
func (T) Read(b []byte) (n int, err error)
```

```go

// https://cs.opensource.google/go/go/+/refs/tags/go1.20.5:src/io/io.go;l=55
// https://cs.opensource.google/go/go/+/master:src/bytes/reader.go;l=38?q=Read%5C(%5Cw%2B%5Cs%5C%5B%5C%5Dbyte%5C)&ss=go%2Fgo

type R struct {
	s string // stored string
	i int64  // current reading index
}

func (r *R) Read(b []byte) (n int, err error) {
	if r.i >= int64(len(r.s)) {
		// End
		return 0, io.EOF
	}
	// copy elements from a source slice (r.s) into destination slice (b)
	n = copy(b, r.s[r.i:])
	// update current index
	r.i += int64(n)
	// naked return (n, err)
	return
}

func main() {
	r := R{s: "Hello World!"}
	b := make([]byte, 8)
	for {
		n, err := r.Read(b)
		if n > 0 {
			fmt.Println(fmt.Sprintf("got %v bytes", n))
			// note that copy only copies bytes from source to destination
			// so second cycle when n is 4 - first 4 bytes of b were overwritten
			// and the last 4 bytes were not - resulting in b value of:
			// "rld!o Wo" ( 4 new and 4 old bytes )
			// that is why we only use n bytes from b
			fmt.Println(fmt.Sprintf("%q\n", b[:n]))
		}
		if err == io.EOF {
			fmt.Println("All Done")
			break
		}
	}
	/*
		got 8 bytes
		"Hello Wo"

		got 4 bytes
		"rld!"

		All Done
	*/
}
```

A common pattern is an io.Reader that wraps another io.Reader,
modifying the stream in some way.

```go
// https://go.dev/tour/methods/23
// https://github.com/jreisinger/gokatas/blob/v0.24.1/rot13/rot13.go
func (r rot13Reader) Read(b []byte) (n int, err error) {
	n, err = r.r.Read(b)
	for i, v := range b[:n] {
		b[i] = rot13(v)
	}
	return
}
```

#### Images interface

[Package image](https://pkg.go.dev/image#Image) defines the Image interface:

- https://go.dev/tour/methods/24

```go
type Image interface {
    ColorModel() color.Model
    Bounds() Rectangle
    At(x, y int) color.Color
}
```

### Generics

Go functions can be written to work on multiple types using type parameters.
The type parameters of a function appear between brackets,
before the function's arguments.

```go
func Index[T comparable](s []T, x T) int
```

In addition to generic functions,
Go also supports generic types.
A type can be parameterized with a type parameter,
which could be useful for implementing generic data structures.

```go
type List[T any] struct {
	next *List[T]
	val  T
}
```

```go
type GenericList[T interface{ ~int | ~string | ~bool }] []T

func (l GenericList[T]) last() T {
	if len(l) == 0 {
		var zeroValue T
		return zeroValue
	}
	return l[len(l)-1]
}

func main() {
	i := GenericList[int]{}
	fmt.Println(i.last()) // 0
	b := GenericList[bool]{}
	fmt.Println(b.last()) // false
}
```

use `~` for [Underlying type](https://go.dev/ref/spec#Underlying_types)

```go
type NUMBA int
type ThisIncludesNumba interface{ ~int | ~string | ~bool }
type ThisDoesNot interface{ int | string | bool }
```

### Type assertion

```go
type shape interface {
	area() float64
}

type rect struct {
	width, height float64
}
type circle struct {
	radius float64
}

func (r rect) area() float64 {
	return r.width * r.height
}
func (r circle) area() float64 {
	return math.Pi * r.radius * r.radius
}

func printCircleArea(e shape) {
	e, ok := e.(circle)
	if !ok {
		log.Fatal("item is not a circle")
	}
	fmt.Println(e.area())
}

func main() {
	printCircleArea(circle{radius: 1})         // 3.141592653589793
	printCircleArea(rect{width: 1, height: 1}) // item is not a circle
}
```

## Goroutines

A goroutine is a lightweight thread managed by the Go runtime.

```go
package main

import (
	"fmt"
	"time"
)

func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}

func main() {
	go say("world")
	say("hello")
}
```

```go
func main() {
	// in go the program will exit when the main gourutine finishes
	// regardless of what any other goroutines might be doing

	// by running both functions as goroutines
	// programm will exit before either of these execute
	go say("world")
	go say("hello")

	// one common but not the best way to get around this
	// is to make main wait for user input
	// fmt.ScanLn()
}
```

### WaitGroup

Better solition is to use [WaitGroup](https://pkg.go.dev/sync#WaitGroup)

```go
package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	// A WaitGroup waits for a collection of goroutines to finish.
	var wg sync.WaitGroup

	// add one to the WaitGroup
	wg.Add(1)
	go func() {
		// when function finishes, decrement the WaitGroup counter by one
		defer wg.Done()
		count("sheep", 5)
	}()

	// add one to the WaitGroup
	wg.Add(1)
	go func() {
		// when function finishes, decrement the WaitGroup counter by one
		defer wg.Done()
		count("fish", 10)
	}()

	// Wait blocks until the WaitGroup counter is zero.
	wg.Wait()
}

func count(s string, count int) {
	for i := 1; i <= count; i++ {
		fmt.Println(i, s)
		time.Sleep(time.Millisecond * 500)
	}
}
```

## Defer

A defer statement defers the execution of a function until the surrounding function returns.

The deferred call's arguments are evaluated immediately, but the function call is not executed until the surrounding function returns.

```go
package main

import "fmt"

func main() {
	defer fmt.Println("world") // this is printed last

	fmt.Println("hello") // this is printed first
}
```

Deferred function calls are pushed onto a stack.
When a function returns, its deferred calls are executed in last-in-first-out order.
See https://go.dev/blog/defer-panic-and-recover

```go
package main

import "fmt"

// outputs "counting; done; 9; 8; 7; 6; 5; 4; 3; 2; 1; 0"
func main() {
	fmt.Println("counting")

	for i := 0; i < 10; i++ {
		defer fmt.Println(i)
	}

	fmt.Println("done")
}
```

## channels

 - [Ticker](https://pkg.go.dev/time#Ticker) or [NewTicker](https://pkg.go.dev/time#NewTicker) - returns a channel that sends event at desired interval
 - [After](https://pkg.go.dev/time#After) - waits for the duration to elapse and then sends the current time on the returned channel
 - [Sleep](https://pkg.go.dev/time#Sleep) - blocks goroutine for at least a specified time

```go
package main

import (
	"fmt"
)

func sum(c chan int) {
	// when taking a value from the channel
	// code is blocked until a variable is sent through the channel
	x := <- c
	y := <- c
	c <- x + y
}

func main() {
	// func make(t Type, size ...IntegerType) Type
	c := make(chan int)
	go sum(c)
	c <- 2
	c <- 3
	r := <-c
	fmt.Println(r) // 5
}
```

```go
// fixed length channels
// using separate input and output channels
package main

import (
	"fmt"
)

func sum(input chan int, output chan int) {
	// note: forever loop is fine as code waits
	// for chanel input to continue
	for {
		x := <-input
		y := <-input
		fmt.Println(fmt.Sprintf("%d+%d=%d", x, y, x+y))
		output <- x + y
	}
}

func main() {
	input := make(chan int, 6)
	output := make(chan int, 3)
	go sum(input, output)

	input <- 1
	input <- 2
	input <- 3
	input <- 4
	input <- 5
	input <- 6

	fmt.Println(fmt.Sprintf("sum #1 %d", <-output))
	fmt.Println(fmt.Sprintf("sum #2 %d", <-output))
	fmt.Println(fmt.Sprintf("sum #3 %d", <-output))
	/*
		1+2=3
		3+4=7
		5+6=11
		sum #1 3
		sum #2 7
		sum #3 11
	*/
}
```

You can also mark channels as `read-only` or `write-only`
```go
// you can only READ from input
// you can only WRITE to output
func sum(input <-chan int, output chan<- int) {
```

You don't need to assign consumed value to a variable

```go
func consumeAndIgnore(input chan int) {
	for {
		<-input
	}
}
```

### concurrent workers

```go
package main

import (
	"fmt"
	"math"
	"math/rand"
)

type primemsg struct {
	num     int
	isPrime bool
}

const TEST_LEN = 100
const NUM_WORKERS = 3

func isPrime(workerId int, input chan primemsg, output chan primemsg) {
	i := 0
	for {
		msg := <-input
		num := msg.num
		fmt.Println("worker #", workerId, "is testing", num)
		sq_root := int(math.Sqrt(float64(num)))
		for i = 2; i <= sq_root; i++ {
			if num%1 == 0 {
				msg.isPrime = false
				output <- msg
				break
			}
		}
		if i > sq_root {
			msg.isPrime = true
			output <- msg
		}
	}
}

func main() {
	msg := primemsg{}

	// create channels
	// NOTE: setting sizse so neitehr filling would be blocked by processing
	//       nor processing be blocked by filling
	input := make(chan primemsg, TEST_LEN)
	output := make(chan primemsg, TEST_LEN)

	// create workers
	for i := 0; i < NUM_WORKERS; i++ {
		go isPrime(i, input, output)
	}

	// fill the input queue
	for i := 0; i < TEST_LEN; i++ {
		msg.num = rand.Intn(1000000) + 1000000
		input <- msg
	}

	// read the answers
	for i := 0; i < TEST_LEN; i++ {
		msg = <-output
		fmt.Println(msg)
	}
}
```

### End loop by closing the channel

```go
package main

import (
	"fmt"
	"time"
)

func main() {
	c := make(chan string)
	go count("Hello", 5, c)

	/*
	// if a "buffered" channel is closed "open" value
	// will be true until the channel is emptied out
	for {
		msg, open := <-c
		if !open {
			break
		}
		fmt.Println(msg)
	}
	*/
	for msg := range c {
		fmt.Println(msg)
	}
}

func count(s string, count int, c chan string) {
	for i := 1; i <= count; i++ {
		c <- s
		time.Sleep(time.Millisecond * 500)
	}
	close(c)
}
```

Note that both sending and receiving a message from a channel is a blocking action

Therefore sending and receiving need to happen in a different goroutine

```go
// This will fail with fatal error
// fatal error: all goroutines are asleep - deadlock!
// because sending the message (c <- "test") blocks the code
// and execution never reaches receiving part
func main() {
	c := make(chan string)
	c <- "test"

	msg := <-c
	fmt.Println(msg)
}

// if you create the channel with a fixed size it won't block untill it's full
func main() {
	c := make(chan string, 1)
	// assigning one value works (as channel has 1 item length of buffer)
	c <- "test"

	// assigning a second value would result in deadloc error
	// as code never reaches channel comsumer
	// c <- "test"

	msg := <-c
	fmt.Println(msg)
}
```

### select

_select_ lets you wait on multiple channel operations.

```go
func main() {
	c1 := make(chan string)
	c2 := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			c1 <- "Every 500ms"
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 2)
			c2 <- "Every 2s"
		}
	}()

	for {
		/*
			// both messages would appear every 2 seconds
			// as c2 will block the code until a new message arrives
			fmt.Println(<-c1)
			fmt.Println(<-c2)
		*/
		select {
		case msg1 := <-c1:
			fmt.Println(msg1)
		case msg2 := <-c2:
			fmt.Println(msg2)
		}
	}
}
```

### Mutex

a mutual exclusion lock
- https://pkg.go.dev/sync#Mutex
- https://gobyexample.com/mutexes

```go
package main

import (
	"fmt"
	"sync"
	"time"
)

var lock sync.Mutex

func action(instance string, std chan<- string) {
	lock.Lock()
	defer lock.Unlock()
	std <- fmt.Sprint(instance, 1)
	time.Sleep(time.Second * 1)
	std <- fmt.Sprint(instance, 2)
}

func main() {
	std := make(chan string)
	go action("a", std)
	go action("b", std)

	for i := 0; i < 4; i++ {
		fmt.Println(<-std)
	}
}
```

```go
package main

import (
	"fmt"
	"sync"
)

const CONCURRENT_JOBS = 100

// with increment value you'll lose some numbers
type Counter struct {
	mu    sync.Mutex
	value int
}

// when trying to modify a map from multiple goroutines you can trigger:
// fatal error: concurrent map writes
type Container struct {
  mu       sync.Mutex
  counters map[string]int
}

func (c *Counter) add() {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.value++
}

func main() {
	var wg sync.WaitGroup
	c := Counter{}

	doIncrement := func(n int) {
		for i := 0; i < n; i++ {
			c.add()
		}
		wg.Done()
	}

	wg.Add(CONCURRENT_JOBS)
	for i := 0; i < CONCURRENT_JOBS; i++ {
		go doIncrement(1000)
	}
	wg.Wait()
	fmt.Println(c.value)
}
```

#### RWMutex

- https://pkg.go.dev/sync#RWMutex

A RWMutex is a reader/writer mutual exclusion lock.
The lock can be held by an arbitrary number of readers or a single writer.

- any number of (read) `RLocks` may be accumulated
- when (write) `Lock` is called it blocks until all Rlocks are released
- from calling `Lock` until calling `Unlock` all `Rlocks` are blocked

```go
func main() {
	var lock sync.RWMutex

	readData := func(thread int) {
		for {
			log.Println("Requesting lock for reading, thread", thread)

			lock.RLock()
			log.Println("Locked for reading, thread", thread)
			time.Sleep(time.Second * 1)

			log.Println("Unlocking reading lock, thread", thread)
			lock.RUnlock()
			time.Sleep(time.Second * 1)
		}
	}
	writeData := func(thread int) {
		for {
			time.Sleep(time.Second * 5)
			log.Println("------------------- REQUESTING WRITELOCK", thread)
			lock.Lock()
			log.Println("------------------- WRITELOCK START", thread)

			time.Sleep(time.Second * 5)

			lock.Unlock()
			log.Println("------------------- WRITELOCK END", thread)
		}
	}

	for i := 1; i <= 5; i++ {
		time.Sleep(time.Second * 1)
		go readData(i)
	}
	for i := 1; i <= 2; i++ {
		go writeData(i)
	}

	fmt.Scanln()
}
```

## Building

- [go build vs go install](https://stackoverflow.com/a/30612612/7630417)
- debug symbols 
  - [linker options](https://pkg.go.dev/cmd/link) you can also get them by using `go build -ldflags="-help"`
```bash
# with debug symbols
RUN go build -o app .
# without debug symbols
RUN go build -ldflags "-s -w" -o app .
```

with `CGO_ENABLED=0` you got a staticaly-linked binary  (https://en.wikipedia.org/wiki/Static_build) so it will run without any external dependencies (you can buld your dockers from 'scratch' image) Like: 
  - https://github.com/s0rg/microapp/blob/master/Dockerfile.scratch
  - https://chemidy.medium.com/create-the-smallest-and-secured-golang-docker-image-based-on-scratch-4752223b7324


