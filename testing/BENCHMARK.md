# BENCHMARK

- https://pkg.go.dev/testing#B
- https://blog.logrocket.com/benchmarking-golang-improve-function-performance/

```go
func BenchmarkRomanToInt(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RomanToInt("MCMXCIV")
	}
}
```

```bash
go test -bench=.
# limits the number of operating system threads that can execute user-level Go code simultaneously
GOMAXPROCS=2 go test -bench=.
# or
go test -bench=. -cpu 2
# run only benchmarks, skip all tests
go test -bench=. -skip=Test. -v
# run a specified benchmark
go test -bench=BenchmarkRomanToInt -v
```
see https://pkg.go.dev/cmd/go#hdr-Testing_flags  for details

```
goos: linux
goarch: amd64
pkg: romanToInt
cpu: Intel(R) Core(TM) i7-6700K CPU @ 4.00GHz
BenchmarkRomanToInt-8            5816616               214.1 ns/op
PASS
ok      romanToInt      1.460s
```
- `goos` - operating system
- `goarch` - architecture
- `pkg` - package
- `cpu` - CPU
- `BenchmarkRomanToInt-8` - name of the benchmark function `-` number of CPUs used to run the benchmark (can be limited by `GOMAXPROCS`)
- `5816616` - total number of times the loop was executed
- `214.1 ns/op` -  average amount of time each iteration took (nanoseconds per operation)
