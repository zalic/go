# testing

- https://pkg.go.dev/testing
- https://go.dev/doc/tutorial/add-a-test
- https://pkg.go.dev/cmd/go#hdr-Test_packages
- https://pkg.go.dev/cmd/go#hdr-Testing_flags
- https://medium.com/rate-engineering/go-test-your-code-an-introduction-to-effective-testing-in-go-6e4f66f2c259

[[_TOC_]]

## naming
basename(filename) + "_test.go"

`foo.go` test would bee `foo_test.go`

## package

- in the same package
  - `package foo` - `./foo.go`
  - `package foo` - `./foo_test.go`
- in test package
  - `package foo` - `./foo.go`
  - `package foo_test` - `./foo_test.go`

## test case

- naming
  - Start with `Test`
  - It is common ( but not required ) to follow that up with the function name, and optionally some description
  - `TestFoo`
  - `TestFooDoesBoo`
  - `TestFoo_Does_Boo`
  - `Test_Foo_Does_Boo`

```go
package foo

import "testing"

func TestFoo(t *testing.T) {

}
```

## testing interface

https://pkg.go.dev/testing#pkg-index

### logging

messages will be printed out if `-v` flag is used or if the test fails

```go
t.Log("message")
t.Logf("message #%d", 1)
```

### failing

```go
// Fail marks the function as having failed but continues execution.
t.Fail()
// FailNow marks the function as having failed
// and stops its execution by calling runtime.Goexit
// (which then runs all deferred calls in the current goroutine)
t.FailNow()
// Failed reports whether the function has failed.
t.Logf("current fail status: %t", t.Failed())

// Error is equivalent to Log followed by Fail.
t.Error("message")
// Errorf is equivalent to Logf followed by Fail.
t.Errorf("message #%d", 1)

// Fatal is equivalent to Log followed by FailNow.
t.Fatal("message")
// Fatalf is equivalent to Logf followed by FailNow.
t.Fatalf("message #%d", 1)
```

### Run

If a benchmark has sub-benchmarks they can be measured like any other benchmark

```go
func TestFooBar(t *testing.T) {
	t.Run("Foo", func(t *testing.T) {
		t.Error("Error in Foo")
	})
	t.Run("Bar", func(t *testing.T) {
		t.Error("Error in Bar")
	})
}
```

```
=== RUN   TestFooBar
=== RUN   TestFooBar/Foo
    romanToInt_test.go:25: Error in Foo
=== RUN   TestFooBar/Bar
    romanToInt_test.go:28: Error in Bar
--- FAIL: TestFooBar (0.00s)
    --- FAIL: TestFooBar/Foo (0.00s)
    --- FAIL: TestFooBar/Bar (0.00s)
```

### Skip

SkipNow must be called from the goroutine running the test,
not from other goroutines created during the test.
Calling SkipNow does not stop those other goroutines.

```go
// Marks the test as SKIP
t.Skip()
// SkipNow marks the test as having been skipped
// and stops its execution by calling runtime.Goexit
t.SkipNow()
// Skipf is equivalent to Logf followed by SkipNow.
t.Skipf("skipping #%d", 1)
// Skipped reports whether the test was skipped.
t.Logf("current SKIP value: %t", t.Skipped())
```

```go
/*
=== RUN   TestFooBar
romanToInt_test.go:31: LOG:1
romanToInt_test.go:32: Skipping execution after this
--- SKIP: TestFooBar (0.00s)
*/
func TestFooBar(t *testing.T) {
	t.Log("LOG:1")
	t.Skip("Skipping test after this")
	t.Log("LOG:2")
}
```

If a test fails (see Error, Errorf, Fail) and is then skipped, it is still considered to have failed

```go
/*
=== RUN   TestFooBar
    romanToInt_test.go:29: LOG:1
    romanToInt_test.go:30: there was an error
    romanToInt_test.go:31: Skipping execution after this
--- FAIL: TestFooBar (0.00s)
*/
func TestFooBar(t *testing.T) {
	t.Log("LOG:1")
	t.Error("there was an error")
	t.Skip("Skipping execution after this")
	t.Log("LOG:2")
}
```

### Cleanup

Cleanup registers a function to be called when the test (or subtest) and all its subtests complete.
Cleanup functions will be called in last added, first called order.

```go
type Counter struct {
	mu    sync.Mutex
	value int
}

func TestFooBar(t *testing.T) {
	c := Counter{}

	t.Run("Add #1", func(t *testing.T) {
		c.value++
		t.Logf("Add #1, %d", c.value)
	})
	t.Run("Add #2", func(t *testing.T) {
		c.value++
		t.Logf("Add #2, %d", c.value)
	})
	t.Cleanup(func() {
		c.mu.Lock()
		defer c.mu.Unlock()
		c.value--
		t.Logf("Cleanup #1, %d", c.value)
	})
	t.Cleanup(func() {
		c.mu.Lock()
		defer c.mu.Unlock()
		c.value--
		t.Logf("Cleanup #2, %d", c.value)
	})
}
```
```
=== RUN   TestFooBar
=== RUN   TestFooBar/Add_#1
    romanToInt_test.go:36: Add #1, 1
=== RUN   TestFooBar/Add_#2
    romanToInt_test.go:42: Add #2, 2
=== NAME  TestFooBar
    romanToInt_test.go:54: Cleanup #2, 1
    romanToInt_test.go:48: Cleanup #1, 0
--- PASS: TestFooBar (0.00s)
    --- PASS: TestFooBar/Add_#1 (0.00s)
    --- PASS: TestFooBar/Add_#2 (0.00s)
```

```go
type Counter struct {
	mu    sync.Mutex
	value int
}

func TestFooBar(t *testing.T) {
	c := Counter{}

	t.Cleanup(func() {
		c.mu.Lock()
		defer c.mu.Unlock()
		c.value--
		t.Logf("Cleanup #1, %d", c.value)
	})
	t.Cleanup(func() {
		c.mu.Lock()
		defer c.mu.Unlock()
		c.value--
		t.Logf("Cleanup #2, %d", c.value)
	})

	t.Run("Add #1", func(t *testing.T) {
		c.value++
		t.Logf("Add #1, %d", c.value)
	})
	t.Run("Add #2", func(t *testing.T) {
		c.value++
		t.Logf("Add #2, %d", c.value)
	})
}
```

```
=== RUN   TestFooBar
=== RUN   TestFooBar/Add_#1
    romanToInt_test.go:47: Add #1, 1
=== RUN   TestFooBar/Add_#2
    romanToInt_test.go:51: Add #2, 2
=== NAME  TestFooBar
    romanToInt_test.go:42: Cleanup #2, 1
    romanToInt_test.go:36: Cleanup #1, 0
--- PASS: TestFooBar (0.00s)
    --- PASS: TestFooBar/Add_#1 (0.00s)
    --- PASS: TestFooBar/Add_#2 (0.00s)
```

```go
type Counter struct {
	mu    sync.Mutex
	value int
}

func TestFooBar(t *testing.T) {
	c := Counter{}

	t.Run("Add #1", func(t *testing.T) {
		c.mu.Lock()
		defer c.mu.Unlock()
		c.value++
		t.Logf("Add #1, %d", c.value)
		t.Cleanup(func() {
			c.mu.Lock()
			defer c.mu.Unlock()
			c.value--
			t.Logf("Cleanup #1, %d", c.value)
		})
	})
	t.Run("Add #2", func(t *testing.T) {
		c.mu.Lock()
		defer c.mu.Unlock()
		c.value++
		t.Logf("Add #2, %d", c.value)
		t.Cleanup(func() {
			c.mu.Lock()
			defer c.mu.Unlock()
			c.value--
			t.Logf("Cleanup #2, %d", c.value)
		})
	})
}
```
```
=== RUN   TestFooBar
=== RUN   TestFooBar/Add_#1
    romanToInt_test.go:36: Add #1, 1
    romanToInt_test.go:41: Cleanup #1, 0
=== RUN   TestFooBar/Add_#2
    romanToInt_test.go:48: Add #2, 1
    romanToInt_test.go:53: Cleanup #2, 0
--- PASS: TestFooBar (0.00s)
```

### Parallel

https://pkg.go.dev/testing#T.Parallel

```go
// signals that this test is to be run in parallel with (and only with) other parallel tests
t.Parallel()
```

```go
func TestFooBar(t *testing.T) {
	c := 0

	t.Run("Add #1", func(t *testing.T) {
		t.Parallel()
		v := c
		t.Logf("Add #1 got value %d", v)
		time.Sleep(time.Second * 3)
		c = v + 1
	})
	t.Run("Add #2", func(t *testing.T) {
		t.Parallel()
		v := c
		t.Logf("Add #2 got value %d", v)
		time.Sleep(time.Second * 3)
		c = v + 1
	})
	t.Cleanup(func() {
		t.Logf("result %d", c)
	})
}
```

```
=== RUN   TestFooBar
=== RUN   TestFooBar/Add_#1
=== PAUSE TestFooBar/Add_#1
=== RUN   TestFooBar/Add_#2
=== PAUSE TestFooBar/Add_#2
=== CONT  TestFooBar/Add_#1
    foobar_test.go:14: Add #1 got value 0
=== CONT  TestFooBar/Add_#2
    foobar_test.go:21: Add #2 got value 0
=== NAME  TestFooBar
    foobar_test.go:26: result 1
```
