package main

import (
	"encoding/json"
	"fmt"
)

type LayoutBar struct {
	// Keys MUST be capitalized
	Child string
}
type Layout struct {
	Foo     string
	Bar     LayoutBar
	Renamed []string `json:"arr"`
}

func main() {
	var s = `{"foo":"foo","bar":{"child":"bar"},"arr":["a","b","c"],"unmapped":"this will not be seen"}`
	var data Layout

	if err := json.Unmarshal([]byte(s), &data); err != nil {
		panic(err)
	}
	fmt.Println(data)           // {foo {bar} [a b c]}
	fmt.Println(data.Foo)       // foo
	fmt.Println(data.Bar)       // {bar}
	fmt.Println(data.Bar.Child) // bar
	fmt.Println(data.Renamed)   // ["a","b","c"]

	for _, item := range data.Renamed {
		fmt.Println(item)
		/*
			a
			b
			c
		*/
	}
}
