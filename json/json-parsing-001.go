package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	var s = `{"foo":"foo","bar":{"child":"bar"}}`
	var data map[string]interface{}

	if err := json.Unmarshal([]byte(s), &data); err != nil {
		panic(err)
	}
	fmt.Println(data) // map[bar:map[child:bar] foo:foo]

	for key, value := range data {
		fmt.Println("key", key, "value", value)
		// key foo value foo
		// key bar value map[child:bar]
	}
	fmt.Println(data["foo"]) // foo
	fmt.Println(data["bar"]) // map[child:bar]
	// but further dept requires extra indexing as
	// invalid operation: cannot index data["bar"] (map index expression of type interface{})
	// fmt.Println(data["bar"]["child"])

}
