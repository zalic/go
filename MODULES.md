# Modules
Go dependencies

[[_TOC_]]

## refs
- [Go Modules Reference](https://go.dev/ref/mod)
- **Tutorials**
  - [Tutorial: Create a Go module](https://go.dev/doc/tutorial/create-module)
  - [Organizing a Go module / module layout](https://go.dev/doc/modules/layout)
  - [Managing dependencies](https://go.dev/doc/modules/managing-dependencies)
  - [Package names](https://go.dev/blog/package-names) (naming conventions of modules/packages)
  - [Tutorial: Getting started with multi-module workspaces](https://go.dev/doc/tutorial/workspaces)
  - https://www.digitalocean.com/community/tutorials/how-to-use-go-modules
  - [DigitalOcean: How to Use Go Modules](https://www.digitalocean.com/community/tutorials/how-to-use-go-modules)
- it is usual to name a module by its remote path `go mod init github.com/<user>/<mod>`

## Base

`go.mod`
```go
module demo
```

`main.go`
```go
package main

import (
	"fmt"
	"demo/sub"
)

func main() {
	x := sub.GetNumber()
	fmt.Println("Hello", x)
}
```

`sub/sub.go`

```go
package sub

// only functions starting with a capital letter
// can be called outside the same package
func GetNumber() int {
	return 6
}
```

## Local development

### workspace

```bash
mkdir work
cd work
```
```bash
mkdir foobar
cd foobar
go mod init gitlab.com/zalic/foobar
```
`main.go`
```go
package foobar

func Foobar() string {
        return "Foobar"
}
```
```bash
cd ..
mkdir test
go mod init gitlab.com/zalic/test
```
`go.mod`
```
module gitlab.com/zalic/test

go 1.20

require (
	gitlab.com/zalic/foobar v0.0.0
)
```
`main.go`
```go
package main

import (
  "fmt"
  "gitlab.com/zalic/foobar"
)

func main() {
  fmt.Println(foobar.Foobar())
}
```

```bash
cd ..
go work init foobar test
```
this creates a `go.work` file
```
go 1.20

use (
        ./foobar
        ./test
)
```
```bash
$ go run gitlab.com/zalic/test
Foobar
$ cd test
$ cd go run main.go
Foobar
```

### replace directive

```bash
mkdir foobar
cd foobar
go mod init gitlab.com/zalic/foobar
```
`main.go`
```go
package foobar

func Foobar() string {
        return "Foobar"
}
```
```bash
cd ..
mkdir test
go mod init test
```
`go.mod`
```
module test

go 1.20

replace (
	gitlab.com/zalic/foobar v0.0.0 => ../foobar
)

require (
	gitlab.com/zalic/foobar v0.0.0
)
```
`main.go`
```go
package main

import (
    "fmt"
    "gitlab.com/zalic/foobar"
)

func main() {
    fmt.Println(foobar.Foobar())
}
```
