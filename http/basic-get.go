package main

import (
	"fmt"
	"io"
	"net/http"
)

func Get(url string) (string, error) {
	// https://pkg.go.dev/net/http#Client.Get
	// https://pkg.go.dev/net/http#Response
	resp, err := http.Get(url)

	if err != nil {
		return "", err
	}
	fmt.Println("Status", resp.Status)                     // string "200 OK"
	fmt.Println("StatusCode", resp.StatusCode)             // int "200"
	fmt.Println("Proto", resp.Proto)                       // string "HTTP/1.1"
	fmt.Println("ProtoMajor", resp.ProtoMajor)             // int 1
	fmt.Println("ProtoMinor", resp.ProtoMinor)             // int 1
	fmt.Println("Header", resp.Header)                     // Header ( map )
	fmt.Println("Header", resp.Header.Get("Content-Type")) // text/html
	fmt.Println("ContentLength", resp.ContentLength)       // int64 "-1"
	fmt.Println("TransferEncoding", resp.TransferEncoding) // []string "[]"
	fmt.Println("Close", resp.Close)                       // bool false
	fmt.Println("Uncompressed", resp.Uncompressed)         // bool true
	fmt.Println("Trailer", resp.Trailer)                   // Header map[]
	//fmt.Println("Request", resp.Request)                   // *Request
	//fmt.Println("TLS", resp.TLS)                           // *tls.ConnectionState

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)

	return fmt.Sprintf("%s", body), nil
}

func main() {
	html, err := Get("http://nofish.eu")
	if err != nil {
		panic(err)
	}
	fmt.Println(html)
}
